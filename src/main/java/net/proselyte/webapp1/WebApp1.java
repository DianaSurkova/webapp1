package net.proselyte.webapp1;

import net.proselyte.webapp1.DTO.DayDTO;
import net.proselyte.webapp1.DTO.OrderDTO;
import net.proselyte.webapp1.DTO.WaiterDTO;
import net.proselyte.webapp1.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class WebApp1 {
    @Autowired
    private WaiterService waiterService;
    @Autowired
    private DayService dayService;
    @Autowired
    private OrderService orderService;

    public static void main(String[] args) {
        SpringApplication.run(WebApp1.class);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void Fill(){

        WaiterDTO waiter1 = new WaiterDTO();
        waiter1.setFirstName("Alina");
        waiter1.setLastName("Pavlova");
        waiterService.save(waiter1);

        List<String> waiters_names = new ArrayList<>();
        waiters_names.add("Alina Pavlova");
        waiters_names.add("Petr Perviy");

        DayDTO day1 = new DayDTO();
        LocalDate d1 = LocalDate.of(2021,7,13);
        day1.setDate(d1);
        day1.setSpecialOffers(true);
        day1.setWaiters_names(waiters_names);
        dayService.save(day1);

        DayDTO day2 = new DayDTO();
        LocalDate d2 = LocalDate.of(2021,7,14);
        day2.setDate(d2);
        day2.setSpecialOffers(false);
        day2.setWaiters_names(waiters_names);
        dayService.save(day2);

        OrderDTO ord1 = new OrderDTO();
        ord1.setDate(d2);
        ord1.setWaiter_id(new Long(1));
        ord1.setPrice(1000);
        orderService.save(ord1);

    }
}

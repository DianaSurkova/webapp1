package net.proselyte.webapp1.rest;

import net.proselyte.webapp1.DTO.OrderDTO;
import net.proselyte.webapp1.model.Order;
import net.proselyte.webapp1.model.Waiter;
import net.proselyte.webapp1.service.DayService;
import net.proselyte.webapp1.service.OrderService;
import net.proselyte.webapp1.service.WaiterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/orders/")
public class OrderRestController {

    @Autowired
    private OrderService orderService;
    @Autowired
    private WaiterService waiterService;
    @Autowired
    private DayService dayService;

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OrderDTO> getOrder(@PathVariable("id") Long orderId) {
        if (orderId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        OrderDTO order = this.orderService.getById(orderId);

        if (order == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OrderDTO> saveOrder(@RequestBody @Valid OrderDTO order) {

        //нельзя добавить заказ без даты или официанта\с датой или официантом, которых нет в базе
        if ((order == null)||(order.getDate() == null)||(order.getWaiter_id() == null)||(dayService.getDayEntityByDate(order.getDate())==null)||(waiterService.getEntityById(order.getWaiter_id())==null)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        OrderDTO res = this.orderService.save(order);
        return new ResponseEntity<>(res, HttpStatus.CREATED);
    }


    @RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Order> deleteOrder(@PathVariable("id") Long id) {
        OrderDTO order = this.orderService.getById(id);

        if (order == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        this.orderService.delete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<OrderDTO>> getAllOrders() {
        List<OrderDTO> order = this.orderService.getAll();

        if (order.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(order, HttpStatus.OK);
    }
}
/*
    @RequestMapping(value = "", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Order> updateOrder(@RequestBody @Valid Order order, UriComponentsBuilder builder) {

        if (order == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        this.orderService.save(order);

        return new ResponseEntity<>(order, HttpStatus.OK);
    }
 */

package net.proselyte.webapp1.rest;

import net.proselyte.webapp1.DTO.WaiterDTO;
import net.proselyte.webapp1.model.Waiter;
import net.proselyte.webapp1.service.WaiterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/waiters/")
public class WaiterRestController {

    @Autowired
    private WaiterService waiterService;

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<WaiterDTO> getWaiter(@PathVariable("id") Long waiterId) {
        if (waiterId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        WaiterDTO waiter = this.waiterService.getById(waiterId);

        if (waiter == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(waiter, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<WaiterDTO> saveWaiter(@RequestBody @Valid WaiterDTO waiter) {

        if (waiter == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        WaiterDTO res = this.waiterService.save(waiter);
        return new ResponseEntity<>(res, HttpStatus.CREATED);

    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Waiter> deleteWaiter(@PathVariable("id") Long id) {
        WaiterDTO waiter = this.waiterService.getById(id);

        if (waiter == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        this.waiterService.delete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<WaiterDTO>> getAllWaiters() {
        List<WaiterDTO> waiter = this.waiterService.getAll();

        if (waiter.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(waiter, HttpStatus.OK);
    }
}
/*
    @RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<WaiterDTO> updateWaiter(@PathVariable("id") Long waiterId, @RequestBody @Valid WaiterDTO waiter, UriComponentsBuilder builder) {

        if (waiter == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (waiterId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        WaiterDTO w = this.waiterService.getById(waiterId);

        if (w == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        this.waiterService.save(waiter);

        return new ResponseEntity<>(waiter, HttpStatus.OK);
    }*/

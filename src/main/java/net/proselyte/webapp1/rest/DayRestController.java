package net.proselyte.webapp1.rest;

import net.proselyte.webapp1.DTO.DayDTO;
import net.proselyte.webapp1.model.Day;
import net.proselyte.webapp1.service.DayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/days/")
public class DayRestController {

    @Autowired
    private DayService dayService;

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<DayDTO> getDay(@PathVariable("id") Long dayId) {
        if (dayId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        DayDTO day = this.dayService.getById(dayId);

        if (day == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(day, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<DayDTO> saveDay(@RequestBody @Valid DayDTO day) {
        if (day == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        DayDTO res = this.dayService.save(day);
        return new ResponseEntity<>(res, HttpStatus.CREATED);
    }


    @RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Day> deleteDay(@PathVariable("id") Long id) {
        DayDTO day = this.dayService.getById(id);

        if (day == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        this.dayService.delete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<DayDTO>> getAllDays() {
        List<DayDTO> day = this.dayService.getAll();

        if (day.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(day, HttpStatus.OK);
    }
}
/*
 @RequestMapping(value = "", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Day> updateDay(@RequestBody @Valid Day day, UriComponentsBuilder builder) {
        if (day == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        this.dayService.save(day);

        return new ResponseEntity<>(day, HttpStatus.OK);
    }
*/

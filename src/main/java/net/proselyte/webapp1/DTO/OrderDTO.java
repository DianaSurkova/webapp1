package net.proselyte.webapp1.DTO;

import lombok.Data;

import java.time.LocalDate;

@Data
public class OrderDTO {
    Long id;
    float price;
    Long waiter_id;
    LocalDate date;
}

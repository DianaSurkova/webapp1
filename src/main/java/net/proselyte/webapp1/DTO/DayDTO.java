package net.proselyte.webapp1.DTO;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class DayDTO {
    Long id;
    LocalDate date;
    boolean specialOffers;
    List<String> waiters_names;
}

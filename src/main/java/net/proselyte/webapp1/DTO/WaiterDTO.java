package net.proselyte.webapp1.DTO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.proselyte.webapp1.model.Day;
import net.proselyte.webapp1.model.Order;

import java.util.Set;

@Data
public class WaiterDTO {
    Long id;
    String firstName;
    String lastName;
    //Set<Day> days;
    //Set<Order> orders;
}

package net.proselyte.webapp1.service;

import lombok.extern.slf4j.Slf4j;
import net.proselyte.webapp1.DTO.WaiterDTO;
import net.proselyte.webapp1.model.Waiter;
import net.proselyte.webapp1.repository.WaiterRepository;
import net.proselyte.webapp1.utils.MappingUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
@Service
public class WaiterService {

    @Autowired
    private WaiterRepository waiterRepository;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private MappingUtils mappingUtils;

    public WaiterDTO getById(Long id) {
        //return waiterRepository.findById(id).orElse(null);
        return mappingUtils.mapToWaiterDto(
                waiterRepository.findById(id)
                        .orElse(null)
        );
    }
    public Waiter getEntityById(Long id) {
        return waiterRepository.findById(id).orElse(null);
    }

    public List<WaiterDTO> getAll() {
        //return waiterRepository.findAll();
        return waiterRepository.findAll().stream()
               .map(mappingUtils::mapToWaiterDto)
               .collect(Collectors.toList());
    }

    @Transactional
    public WaiterDTO save(WaiterDTO waiter) {
        List<WaiterDTO> w = getAll();
        for (WaiterDTO dto : w)
        {
            if (waiter.getFirstName().equals(dto.getFirstName()) && waiter.getLastName().equals(dto.getLastName())){
                return dto;
            }
        }
        Waiter res = em.merge(mappingUtils.mapToWaiterEntity(waiter));
        return mappingUtils.mapToWaiterDto(res);
    }

    public void delete(Long id) {
        waiterRepository.deleteById(id);
    }



}

package net.proselyte.webapp1.service;

import lombok.extern.slf4j.Slf4j;
import net.proselyte.webapp1.DTO.OrderDTO;
import net.proselyte.webapp1.model.Day;
import net.proselyte.webapp1.model.Order;
import net.proselyte.webapp1.model.Waiter;
import net.proselyte.webapp1.repository.OrderRepository;
import net.proselyte.webapp1.utils.MappingUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
@Service
public class OrderService {

    @Autowired
    OrderRepository orderRepository;
    @PersistenceContext
    EntityManager em;
    @Autowired
    private MappingUtils mappingUtils;
    @Autowired
    private WaiterService waiterService;
    @Autowired
    private DayService dayService;

    public OrderDTO getById(Long id) {
        //return orderRepository.findById(id).orElse(null);
        return mappingUtils.mapToOrderDto(
                orderRepository.findById(id)
                        .orElse(null)
        );
    }

    public Order getEntityById(Long id) {
        return orderRepository.findById(id).orElse(null);
    }

    @Transactional
    public OrderDTO save(OrderDTO order) {

        if (order.getId()!= null)
        {//меняем уже существующий
            Order ord = getEntityById(order.getId());
            if (ord != null)
            {
                ord.setPrice(order.getPrice());
                Waiter w = waiterService.getEntityById(order.getWaiter_id());
                if (w != null)
                {
                    ord.setWaiter(w);
                }
                Day d = dayService.getDayEntityByDate(order.getDate());
                if (d != null)
                {
                    ord.setDay(d);
                }
                Order res =  em.merge(ord);
                return mappingUtils.mapToOrderDto(res);
            }
        }
        //сохраняем новый
        Order ord = mappingUtils.mapToOrderEntity(order);
        Order res =  em.merge(ord);
        return mappingUtils.mapToOrderDto(res);
    }

    public void delete(Long id) {
        orderRepository.deleteById(id);
    }

    public List<OrderDTO> getAll() {
        //return orderRepository.findAll();
        return orderRepository.findAll().stream()
                .map(mappingUtils::mapToOrderDto)
                .collect(Collectors.toList());
    }
}

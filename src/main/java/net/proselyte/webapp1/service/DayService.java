package net.proselyte.webapp1.service;

import lombok.extern.slf4j.Slf4j;
import net.proselyte.webapp1.DTO.DayDTO;
import net.proselyte.webapp1.DTO.WaiterDTO;
import net.proselyte.webapp1.model.Day;
import net.proselyte.webapp1.model.Waiter;
import net.proselyte.webapp1.repository.DayRepository;
import net.proselyte.webapp1.utils.MappingUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
@Service
public class DayService {

    @Autowired
    private DayRepository dayRepository;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private MappingUtils mappingUtils;
    @Autowired
    private WaiterService waiterService;

    public DayDTO getById(Long id) {
        //return dayRepository.findById(id).orElse(null);
        return mappingUtils.mapToDayDto(
                dayRepository.findById(id)
                        .orElse(null)
        );
    }
    public Day getDayEntityById(Long id) {
        return dayRepository.findById(id).orElse(null);
    }
    public Day getDayEntityByDate(LocalDate date) {
        List<Day> days = dayRepository.findAll();
        for(Day d : days)
        {
            if (d.getDate().equals(date))
            {
                return d;
            }
        }
        return null;
    }

    @Transactional
    public DayDTO save(DayDTO day) {
        //return em.merge(day);
        List<DayDTO> w = getAll();
        //Day res = em.merge(mappingUtils.mapToDayEntity(day));
        Day res = mappingUtils.mapToDayEntity(day);
        for (DayDTO dto : w)
        {
            if (day.getDate().equals(dto.getDate())){
                res = getDayEntityByDate(day.getDate());
                res.setSpecialOffers(day.isSpecialOffers());
                res.setWaiters(new HashSet<>());
            }
        }
        if (day.getWaiters_names() != null)
        {
            List<String> waiters_names = day.getWaiters_names();
            List<WaiterDTO> waiters = waiterService.getAll();
            Set<Waiter> waitersres = new HashSet<>();;
            String[] parts;
            //boolean flag = false;
            for (String name : waiters_names)
            {
                parts = name.split("\\s");
                for (WaiterDTO dto : waiters)
                {
                    if (dto.getFirstName().equals(parts[0]) && dto.getLastName().equals(parts[1]))
                    {
                        //такое имя уже есть в базе официантов
                        waitersres.add(waiterService.getEntityById(dto.getId()));
                        //flag = true;
                    }
                }
                //if (!flag)
                //{
                    //Waiter waiter2 = new Waiter();
                    //waiter2.setFirstName(parts[0]);
                    //waiter2.setLastName(parts[1]);
                    //waitersres.add(waiter2);
                //}
            }
            res.setWaiters(waitersres);
        }
        res.setSpecialOffers(day.isSpecialOffers());
        Day fres = em.merge(res);
        return mappingUtils.mapToDayDto(fres);
    }

    public void delete(Long id) {
        dayRepository.deleteById(id);
    }

    public List<DayDTO> getAll() {

        //return dayRepository.findAll();
        return dayRepository.findAll().stream()
                .map(mappingUtils::mapToDayDto)
                .collect(Collectors.toList());
    }
}

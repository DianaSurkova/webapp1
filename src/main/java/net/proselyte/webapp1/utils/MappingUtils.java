package net.proselyte.webapp1.utils;

import net.proselyte.webapp1.DTO.DayDTO;
import net.proselyte.webapp1.DTO.OrderDTO;
import net.proselyte.webapp1.DTO.WaiterDTO;
import net.proselyte.webapp1.model.Day;
import net.proselyte.webapp1.model.Order;
import net.proselyte.webapp1.model.Waiter;
import net.proselyte.webapp1.service.DayService;
import net.proselyte.webapp1.service.WaiterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@Component
public class MappingUtils {

    @Autowired
    private WaiterService waiterService;
    @Autowired
    private DayService dayService;

    public WaiterDTO mapToWaiterDto(Waiter entity){
        WaiterDTO dto = new WaiterDTO();
        dto.setId(entity.getId());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        //dto.setDays(entity.getDays());
        //dto.setOrders(entity.getOrders());

        return dto;
    }

    public Waiter mapToWaiterEntity(WaiterDTO dto){
        Waiter entity = new Waiter();
        entity.setId(dto.getId());
        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());

        return entity;
    }
    public DayDTO mapToDayDto(Day entity){
        DayDTO dto = new DayDTO();
        dto.setId(entity.getId());
        dto.setDate(entity.getDate());
        dto.setSpecialOffers(entity.isSpecialOffers());
        List<String> names = new ArrayList<>();
        Set<Waiter> waiters = entity.getWaiters();
        int i = 0;
        for (Waiter w : waiters)
        {
            names.add(w.getFirstName() + " " + w.getLastName());
        }
        dto.setWaiters_names(names);

        return dto;
    }

    public Day mapToDayEntity(DayDTO dto){
        Day entity = new Day();
        entity.setId(dto.getId());
        entity.setDate(dto.getDate());
        //entity.setWaiters(dto.getLastName());

        return entity;
    }

    public OrderDTO mapToOrderDto(Order entity){
        OrderDTO dto = new OrderDTO();
        dto.setId(entity.getId());
        dto.setPrice(entity.getPrice());
        dto.setDate(entity.getDay().getDate());
        dto.setWaiter_id(entity.getWaiter().getId());

        return dto;
    }

    public Order mapToOrderEntity(OrderDTO dto){
        Order entity = new Order();
        entity.setId(dto.getId());
        entity.setPrice(dto.getPrice());
        Waiter w = waiterService.getEntityById(dto.getWaiter_id());
        if (w != null)
        {
            entity.setWaiter(w);
        }
        Day d = dayService.getDayEntityByDate(dto.getDate());
        if (d != null)
        {
            entity.setDay(d);
        }
        return entity;
    }

}

package net.proselyte.webapp1.repository;

import net.proselyte.webapp1.model.Waiter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WaiterRepository extends JpaRepository<Waiter, Long> {
}

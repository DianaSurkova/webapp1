package net.proselyte.webapp1.repository;

import net.proselyte.webapp1.model.Day;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DayRepository extends JpaRepository<Day, Long>{
}

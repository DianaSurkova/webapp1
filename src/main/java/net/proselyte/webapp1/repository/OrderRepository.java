package net.proselyte.webapp1.repository;

import net.proselyte.webapp1.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long>{
}

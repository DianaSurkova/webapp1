package net.proselyte.webapp1.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "orders")
@Getter
@Setter
@ToString
public class Order implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "price")
    protected float price;

    @ManyToOne
    @JoinColumn(name="waiter_id")
    private Waiter waiter;

    @ManyToOne
    @JoinColumn(name="day_id")
    private Day day;

    public void setDay(Day day) {
        this.day = day;
    }
}

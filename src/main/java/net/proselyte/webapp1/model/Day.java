package net.proselyte.webapp1.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "days")
@Getter
@Setter
@ToString
public class Day implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "special_offers")
    private boolean specialOffers;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "Day_Waiter",
            joinColumns = { @JoinColumn(name = "day_id") },
            inverseJoinColumns = { @JoinColumn(name = "waiter_id") }
    )
    private Set<Waiter> waiters = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "day")
    private Set<Order> orders;

}
//@JsonIgnore
//@OneToMany(mappedBy = "day")
//Set<DayWaiters> dayWaiters;
package net.proselyte.webapp1.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "waiters")
@Getter
@Setter
@ToString
public class Waiter  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @JsonIgnore
    @ManyToMany(mappedBy = "waiters")
    private Set<Day> days = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "waiter")
    private Set<Order> orders;

}
//@JsonIgnore
//@OneToMany(mappedBy = "waiter")
//Set<DayWaiters> dayWaiters;